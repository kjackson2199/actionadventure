﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(Animator))]
[ExecuteInEditMode]
public class CharacterMovementComponent : MonoBehaviour
{
    //Movement Component Settings
    [Header("Movement Component Settings")]
    [Range(1, 10)]
    public float WalkSpeed = 4;
    [Range(1, 360)]
    public float TurnRate = 45f;
    [Range(1, 10)]
    public float TurnSensitivity = 1;
    [Range(1, 360)]
    public float LookUpRate = 45f;
    [Range(1, 10)]
    public float LookSensitivity = 1;
    [Range(1, 90)]
    public float MaxLookUpAngle = 80;
    [Range(1, 10)]
    public float RunSpeed = 5;
    [Range(0, 2)]
    public float CrouchSpeed = .5f;
    [Range(0, 5)]
    public float JumpForce = 1;
    [Range(1, 15)]
    public float StaminaFalloffTime = 5;

    public bool UseYawRotationOnMesh = true;

    public float MaxAcceleration = 5;

    //Movement Component Actions
    [HideInInspector]
    public bool bCrouching = false;
    [HideInInspector]
    public bool bRunning = false;
    [HideInInspector]
    public bool bInAir = false;
    [HideInInspector]
    public bool bIsFalling = false;

    //Movement Control
    [Header("Movement Control Settings")]
    public float RunSpeedThreshold = .2f;
    public float WalkSpeedThreshold = .1f;
    [Range(0, 1)]
    public float FallSpeedThreshold = .1f;

    float forwardInput;
    float rightInput;
    float lookUpInput;
    float turnInput;

    bool bUseRunSpeed = false;

    public float groundCheckRange = .4f;
    public float groundCheckHeighOffset;
    public LayerMask groundCheckLayers;

    [Header("Animator Driver Settings")]
    public bool bDisableAnimationUpdate = false;

    [Header("Animation Input Float Bindings")]
    public string AnimationForwardSpeed;
    public string AnimationRightSpeed;

    [Header("Animation Input Boolean Bindings")]
    public string AnimationCrouchingBoolean;
    public string AnimationRunningBoolean;
    public string AnimationInAirBoolean;
    public string AnimationIsFallingBoolean;

    [Header("Animation Trigger Bindings")]
    public string AnimationJumpTrigger;

    Rigidbody body;
    Animator anim;
    CameraController cameraController;

    private void Awake()
    {
        body = GetComponent<Rigidbody>();
        cameraController = GetComponent<CameraController>();
        anim = GetComponent<Animator>();
    }

    private void Update()
    {
        if(!Application.isPlaying)
        {
            cameraController.ApplyYawRotationToCamera = !UseYawRotationOnMesh;
        }
        CharacterMovement();
        GroundCheck();
        FallCheck();
        AnimatorUpdate();
    }

    private void OnDisable()
    {
        forwardInput = 0;
        rightInput = 0;
        turnInput = 0;
        lookUpInput = 0;
    }

    void CharacterMovement()
    {
        Vector3 currentVel = body.velocity;
        Vector3 targetVel;
        if(bUseRunSpeed)
        {
            targetVel = (transform.forward * forwardInput * RunSpeed) + (transform.right * rightInput * RunSpeed);
        }
        else
        {
            targetVel = (transform.forward * forwardInput * WalkSpeed) + (transform.right * rightInput * RunSpeed);
        }

        Vector3 velChange = targetVel - currentVel;
        if(velChange.sqrMagnitude > MaxAcceleration * MaxAcceleration)
        {
            velChange = velChange.normalized * MaxAcceleration;
        }

        velChange = new Vector3(velChange.x, 0, velChange.z);
        
        body.AddForce(velChange, ForceMode.VelocityChange);

        cameraController.AddXControlInput(lookUpInput);
        if(UseYawRotationOnMesh)
        {
            Vector3 targetYawRotation = turnInput * TurnRate * TurnSensitivity * Vector3.up * Time.deltaTime;
            gameObject.transform.Rotate(targetYawRotation, Space.Self);
        }
        else
        {
            cameraController.AddYControlInput(turnInput);
        }
    }

    public void AddMovementInputForward(float AxisInput)
    {
        forwardInput = AxisInput;
    }

    public void AddMovementInputRight(float AxisInput)
    {
        rightInput = AxisInput;
    }

    public void AddYawInput(float AxisInput)
    {
        turnInput = AxisInput;
    }

    public void AddPitchInput(float AxisInput)
    {
        lookUpInput = AxisInput;
    }

    void GroundCheck()
    {
        Vector3 testFront = transform.position + transform.forward * groundCheckRange + transform.up * groundCheckHeighOffset;
        Vector3 testRear = transform.position + transform.forward * groundCheckRange * -1 + transform.up * groundCheckHeighOffset;
        Vector3 testRight = transform.position + transform.right * groundCheckRange + transform.up * groundCheckHeighOffset;
        Vector3 testLeft = transform.position + transform.right * groundCheckRange * -1 + transform.up * groundCheckHeighOffset;

        Debug.DrawLine(testFront, testFront + transform.up * 1, Color.blue);
        Debug.DrawLine(testLeft, testLeft + transform.up * 1, Color.red);
        Debug.DrawLine(testRight, testRight + transform.up * 1, Color.green);
        Debug.DrawLine(testRear, testRear + transform.up * 1, Color.white);

        Collider[] testFrontCollider = Physics.OverlapSphere(testFront, 0.1f, groundCheckLayers);
        Collider[] testRearCollider = Physics.OverlapSphere(testRear, 0.1f, groundCheckLayers);
        Collider[] testRightCollider = Physics.OverlapSphere(testRight, 0.1f, groundCheckLayers);
        Collider[] testLeftCollider = Physics.OverlapSphere(testLeft, 0.1f, groundCheckLayers);

        if(testFrontCollider.Length > 0 || testRearCollider.Length > 0 || testRightCollider.Length > 0 || testLeftCollider.Length > 0)
        {
            bInAir = false;
        }
        else if (testFrontCollider.Length == 0 || testRearCollider.Length == 0 || testRightCollider.Length == 0 || testLeftCollider.Length == 0)
        {
            bInAir = true;
        }
    }

    void FallCheck()
    {
        if(body.velocity.y < FallSpeedThreshold && bInAir)
        {
            bIsFalling = true;
        }
        else if(body.velocity.y >= FallSpeedThreshold)
        {
            bIsFalling = false;
        }
    }

    public void Crouch(bool SetCrouching)
    {
        if(SetCrouching)
        {
            bCrouching = true;
        }
        else
        {
            bCrouching = false;
        }
    }

    public void Run(bool SetRunning)
    {
        if(SetRunning)
        {
            bUseRunSpeed = true;
            bRunning = true;
        }

        else
        {
            bUseRunSpeed = false;
            bRunning = false;
        }
    }

    public void Jump()
    {
        if (bInAir) return;
        anim.SetTrigger(AnimationJumpTrigger);
        body.AddForce(Vector3.up * JumpForce, ForceMode.VelocityChange);
    }

    public void AnimatorUpdate()
    {
        if (bDisableAnimationUpdate) return;
        if(anim.applyRootMotion)
        {
            anim.applyRootMotion = false;
        }
        //Update all float values
        anim.SetFloat(AnimationForwardSpeed, Vector3.Dot(body.velocity, transform.forward));
        anim.SetFloat(AnimationRightSpeed, Vector3.Dot(body.velocity, transform.right));

        //Update all bool values
        anim.SetBool(AnimationCrouchingBoolean, bCrouching);
        anim.SetBool(AnimationInAirBoolean, bInAir);
        anim.SetBool(AnimationIsFallingBoolean, bIsFalling);
        anim.SetBool(AnimationRunningBoolean, bRunning);
    }

    public void SetComponentEnabled(bool Enable)
    {
        enabled = Enable;
    }
}
