﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class SpringArmComponent : MonoBehaviour
{
    public float SpringArmLength = 3;
    public Camera AttachedCamera;
    public LayerMask CameraCollisionLayerMask;
    //public Vector3 ArmOffset;

    void CastArm()
    {
        if (AttachedCamera == null) return;

        RaycastHit hit;
        if (Physics.Raycast(transform.position, transform.forward * -1, out hit, SpringArmLength, CameraCollisionLayerMask))
        {
            AttachedCamera.transform.position = transform.position + (transform.forward * hit.distance * -1);
        }
        else
        {
            AttachedCamera.transform.position = transform.position + (transform.forward * SpringArmLength * -1);
        }
    }

    private void Update()
    {
        if(!Application.isPlaying)
        {
            if (AttachedCamera == null) return;
            AttachedCamera.transform.position = transform.position + (transform.forward * SpringArmLength * -1);
        }
    }

    private void FixedUpdate()
    {
        CastArm();
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawLine(transform.position, transform.position + (transform.forward * SpringArmLength * -1));
    }
}
