﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [Range(1, 360)]
    public float TurnRate = 45f;
    [Range(1, 10)]
    public float TurnSensitivity = 1;
    [Range(1, 360)]
    public float LookUpRate = 45f;
    [Range(1, 10)]
    public float LookSensitivity = 1;
    [Range(1, 90)]
    public float MaxLookUpAngle = 80;

    public bool ApplyYawRotationToCamera = false;

    public enum ECameraMode
    {
        CM_FirstPerson,
        CM_ThirdPerson,
        CM_Other
    }

    public ECameraMode CurrentCameraMode = ECameraMode.CM_ThirdPerson;
    public SpringArmComponent ThirdPersonCameraBoom;
    public GameObject YRoot;
    public GameObject XRoot;
    public Camera Camera;

    float xControlInput;
    float yControlInput;

    private void FixedUpdate()
    {
        CameraMovement();
    }

    void CameraMovement()
    {
        switch(CurrentCameraMode)
        {
            case ECameraMode.CM_FirstPerson:
                FirstPersonTurningMovement();
                break;
            case ECameraMode.CM_ThirdPerson:
                ThirdPersonTurningMovement();
                break;
        }
    }

    void FirstPersonTurningMovement()
    {

    }

    void ThirdPersonTurningMovement()
    {
        if (ThirdPersonCameraBoom == null) return;
        Vector3 targetYawRotation = yControlInput * TurnRate * TurnSensitivity * Vector3.up * Time.deltaTime;
        Vector3 targetPitchRotation = xControlInput * LookUpRate * LookSensitivity * Vector3.right * Time.deltaTime;
        if(ApplyYawRotationToCamera)
        {
            YRoot.transform.Rotate(targetYawRotation, Space.Self);
        }
        XRoot.transform.Rotate(targetPitchRotation, Space.Self);
    }

    public void AddXControlInput(float AxisInput)
    {
        xControlInput = AxisInput;
    }

    public void AddYControlInput(float AxisInput)
    {
        yControlInput = AxisInput;
    }
}
