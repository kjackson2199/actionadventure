﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterMovementComponent))]
public class PlayerController : MonoBehaviour
{
    [Header("Movement Control Settings")]
    public bool bInputDisabled = false;
    public bool bUseFlightComponent = false;
    public bool bUseCombatComponent = false;

    //Input Axis
    [Header("Input Axis Bindings")]
    public string MoveForwardInput = "Vertical";
    public string MoveRightInput = "Horizontal";
    public string LookUpInput = "Mouse Y";
    public bool InvertMouseY = false;
    public string TurnInput = "Mouse X";
    public bool InvertMouseX = false;

    //Input Actions
    [Header("Input Action Bindings")]
    public KeyCode JumpKey = KeyCode.Space;
    public KeyCode RunKey = KeyCode.LeftShift;
    public KeyCode CrouchKey = KeyCode.LeftControl;

    [Header("Movement Components")]
    CharacterMovementComponent BaseMovementComponent;

    enum EControlMode
    {
        CM_RegularMovementControl,
        CM_FlightMode,
        CM_CombatMode
    }

    EControlMode CurrentControlMode = EControlMode.CM_RegularMovementControl;

    private void Awake()
    {
        BaseMovementComponent = GetComponent<CharacterMovementComponent>();
    }

    private void Update()
    {
        if (bInputDisabled) return;

        switch(CurrentControlMode)
        {
            case EControlMode.CM_RegularMovementControl:
                RegularMovementControlUpdate();
                break;
            case EControlMode.CM_FlightMode:
                FlightMovementControlUpdate();
                break;
            case EControlMode.CM_CombatMode:
                CombatMovementControlUpdate();
                break;
        }
    }

    void RegularMovementControlUpdate()
    {
        BaseMovementComponent.AddMovementInputForward(Input.GetAxis(MoveForwardInput));
        BaseMovementComponent.AddMovementInputRight(Input.GetAxis(MoveRightInput));
        if(InvertMouseX)
        {
            BaseMovementComponent.AddYawInput(Input.GetAxis(TurnInput) * -1);
        }
        else
        {
            BaseMovementComponent.AddYawInput(Input.GetAxis(TurnInput));
        }
        if(InvertMouseY)
        {
            BaseMovementComponent.AddPitchInput(Input.GetAxis(LookUpInput) * -1);
        }
        else
        {
            BaseMovementComponent.AddPitchInput(Input.GetAxis(LookUpInput));
        }
        
        if (Input.GetKey(JumpKey))
        {
            BaseMovementComponent.Jump();
        }

        BaseMovementComponent.Run(Input.GetKey(RunKey));
        BaseMovementComponent.Crouch(Input.GetKey(CrouchKey));
    }

    void FlightMovementControlUpdate()
    {
        if (!bUseFlightComponent) return;
    }

    void CombatMovementControlUpdate()
    {
        if (!bUseCombatComponent) return;
    }

    void DisableInput()
    {
        bInputDisabled = true;
    }

    void EnableInput()
    {
        bInputDisabled = false;
    }
}
