﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JessPlayerController : MonoBehaviour
{
    public static JessPlayerController instance;

    public Vector2 speed = Vector2.one;

    public GameObject player;

    Animator anim;
    Rigidbody body;

    float x;
    float z;

    void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        
        anim = GetComponent<Animator>();
        body = GetComponent<Rigidbody>();
    }


    void Update()
    {
        x = Input.GetAxis("Horizontal") * speed.x;
        z = Input.GetAxis("Vertical") * speed.y;

        transform.Rotate(Vector3.up * x * Time.deltaTime);

        anim.SetFloat("Turning", x * Mathf.Deg2Rad);
        anim.SetFloat("Forward", Vector3.Dot(body.velocity, transform.forward));
    }

    private void FixedUpdate()
    {
        Vector3 targetVelocity = transform.forward * z;
        Vector3 velocityChange = targetVelocity - body.velocity;
        // if vC > max : clip it
        body.AddForce(velocityChange, ForceMode.VelocityChange);
    }
}
