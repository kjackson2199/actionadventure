﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorController : MonoBehaviour
{
    //Door Properties
    public bool bDoorLocked = false;
    bool bDoorOpened = false;

    //Animation Settings
    public string doorOpenTriggerName = "DoorOpen";
    public string doorCloseTriggerName = "DoorClose";

    Animator anim;

    private void Start()
    {
        anim = gameObject.GetComponent<Animator>();
        //if(anim == null)
        //{
        //    Debug.Log(gameObject.name + ". " + "Animator does not exist on current object.");
        //}
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<JessPlayerController>())
        {
            Debug.Log("Player entered trigger");
            if (!bDoorLocked && !bDoorOpened)
            {
                bDoorOpened = true;
                if (anim != null)
                {
                    anim.SetTrigger(doorOpenTriggerName);
                }
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if(other.gameObject.GetComponent<JessPlayerController>())
        {
            if(bDoorOpened)
            {
                Debug.Log("Player left trigger");
                bDoorOpened = false;
                if (anim != null)
                {
                    anim.SetTrigger(doorCloseTriggerName);
                }
                
            }
        }
    }
}
