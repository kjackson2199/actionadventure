﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RmecController : MonoBehaviour
{
    public float speed = 1;

    public enum State
    {
        Idle,
        Chase,
        Attack,
    }
    public State state = State.Idle;

    float attackTime = -1;

    bool shoot = false;

    Animator anim;
    Rigidbody body;

    private void Start()
    {
        anim = GetComponent<Animator>();
        body = GetComponent<Rigidbody>();
    }

    void Update()
    {
        anim.SetFloat("turn", Mathf.Deg2Rad);
        anim.SetFloat("forward", body.velocity.magnitude);

        switch (state)
        {
            case State.Idle:
                UpdateIdle();
                break;
            case State.Chase:
                UpdateChase();
                break;
            case State.Attack:
                UpdateAttack();
                break;
        }
    }

    void UpdateIdle ()
    {
        body.velocity = Vector3.zero;
        Vector3 p = JessPlayerController.instance.transform.position;
        if (Vector3.Distance(transform.position, p) < 12)
        {
            state = State.Chase;
        }
    }

    void UpdateChase ()
    {
        Vector3 p = JessPlayerController.instance.transform.position;
        Vector3 dir = p - transform.position;
        dir.y = 0;
        transform.forward = dir;
        Vector3 targetVel = dir.normalized * speed;
        Vector3 velocityChange = targetVel - body.velocity;
        velocityChange.y = 0;
        if (velocityChange.magnitude > 1) velocityChange.Normalize();
        body.AddForce(velocityChange, ForceMode.VelocityChange);

        float d = dir.magnitude;
        if (d < 8)
        {
            anim.SetBool("shoot", true);
            state = State.Attack;
            attackTime = Time.time;
        }
        else if (d > 16)
        {
            state = State.Idle;
        }       
    }

    void UpdateAttack ()
    {
        body.velocity = Vector3.zero;
        if (Time.time - attackTime > 3)
        {
            state = State.Chase;
            anim.SetBool("shoot", false);
        }
    }
}
